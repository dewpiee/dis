package ru.nsu.fit.g15202.vakhrushev.dis.lab.Model;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLHStoreType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Data
@Entity
@Table(name = "nodes")
@TypeDef(name = "hstore", typeClass = PostgreSQLHStoreType.class)
public class Node
{
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column
    private Long version;

    @Column
    private String timestamp;

    @Column
    private Long uid;

    @Column(name = "username")
    private String user;

    @Column
    private Long changeset;

    @Column
    private double lat;

    @Column
    private double lon;

    @Type(type = "hstore")
    @Column(columnDefinition = "hstore")
    private Map<String, String> tags = new HashMap<>();
}
