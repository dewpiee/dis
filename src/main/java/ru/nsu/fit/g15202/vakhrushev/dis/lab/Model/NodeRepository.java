package ru.nsu.fit.g15202.vakhrushev.dis.lab.Model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NodeRepository extends JpaRepository<Node, Long>
{
    @Query
    (
            value = "SELECT *, earth_distance(ll_to_earth(?1,?2), ll_to_earth(lat, lon)) as dist " +
                    "FROM nodes WHERE earth_box(ll_to_earth(?1,?2), ?3) @> ll_to_earth(lat, lon)" +
                    "ORDER by dist",
            nativeQuery = true
    )
    List<Node> getNodesNear(Double lat, Double lon, Double r);
}
