package ru.nsu.fit.g15202.vakhrushev.dis.lab.Parse;

import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.util.ReflectionUtils;
import ru.nsu.fit.g15202.vakhrushev.dis.lab.Model.Node;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NodeParser
{
    private static final String NODE = "node";

    private final ConversionService convert;
    private final XMLStreamReader reader;

    public NodeParser(String path) throws IOException, XMLStreamException
    {
        var input = Files.newInputStream(Paths.get(path));
        var factory = XMLInputFactory.newInstance();

        reader = factory.createXMLStreamReader(input);

        convert = new DefaultConversionService();
    }

    public List<Node> parseNodes(int count) throws XMLStreamException, IllegalAccessException
    {
        var nodes = new ArrayList<Node>();

        for (var ind = 0; ind < count; ind++)
        {
            var nd = this.parseNode();

            if (nd == null)
                break;

            nodes.add(nd);
        }

        return nodes;
    }

    private Node parseNode() throws XMLStreamException, IllegalAccessException
    {
        while (reader.hasNext())
        {
            reader.next();

            if (reader.getEventType() == XMLEvent.START_ELEMENT)
            {
                var elementName = reader.getLocalName();

                if (!elementName.equals(NODE))
                    continue;

                var node = new Node();
                var attrCount = reader.getAttributeCount();

                for (var ind = 0; ind < attrCount; ind++)
                {
                    var attrName = reader.getAttributeLocalName(ind);
                    var attrVal = reader.getAttributeValue(ind);

                    var prop = ReflectionUtils.findField(Node.class, attrName);

                    if (prop != null)
                    {
                        prop.setAccessible(true);
                        prop.set(node, convert.convert(attrVal, prop.getType()));
                    }
                }

                var tags = parseTags();
                node.setTags(tags);

                return node;
            }
        }

        return null;
    }

    private Map<String, String> parseTags() throws XMLStreamException
    {
        var tags = new HashMap<String, String>();

        while (!reader.isEndElement() || !NODE.equals(reader.getLocalName()))
        {
            if (reader.isStartElement() && "tag".equals(reader.getLocalName()))
            {
                var k = reader.getAttributeValue(0);
                var v = reader.getAttributeValue(1);

                tags.put(k, v);
            }

            reader.next();
        }

        return tags;
    }
}
