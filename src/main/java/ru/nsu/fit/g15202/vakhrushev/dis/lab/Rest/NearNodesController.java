package ru.nsu.fit.g15202.vakhrushev.dis.lab.Rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.g15202.vakhrushev.dis.lab.Model.Node;
import ru.nsu.fit.g15202.vakhrushev.dis.lab.Model.NodeRepository;

import java.util.List;

@RestController
public class NearNodesController
{
    private final NodeRepository repo;

    NearNodesController(NodeRepository repository) { repo = repository; }

    @GetMapping("/nodes/near")
    public List<Node> getNodesNear(@RequestParam Double lat, @RequestParam Double lon, @RequestParam Double r)
    {
        return repo.getNodesNear(lat, lon, r);
    }
}
