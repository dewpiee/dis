package ru.nsu.fit.g15202.vakhrushev.dis.lab.Rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.g15202.vakhrushev.dis.lab.Model.Node;
import ru.nsu.fit.g15202.vakhrushev.dis.lab.Model.NodeRepository;

@RestController
public class NodeController
{
    private final NodeRepository repo;

    NodeController(NodeRepository repo) { this.repo = repo; }

    @PostMapping("/nodes")
    public ResponseEntity createNode(@RequestBody Node node)
    {
        if (node == null)
            return
                ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body("Node is null.");

        var savedNode = repo.save(node);

        return
            ResponseEntity
            .status(HttpStatus.OK)
            .body(savedNode + " is successful saved.");
    }

    @GetMapping("/nodes")
    public ResponseEntity getAllNodes()
    {
        var nodes = repo.findAll();

        return
            ResponseEntity
            .status(HttpStatus.OK)
            .body(nodes);
    }

    @GetMapping("/nodes/{id}")
    public ResponseEntity getNode(@PathVariable Long id)
    {
        var node = repo.findById(id);

        if (node.isPresent())
            return
                ResponseEntity
                .status(HttpStatus.OK)
                .body(node.get());

        return
            ResponseEntity
            .status(HttpStatus.NOT_FOUND)
            .body("Node with id = " + id + " not found.");
    }

    @DeleteMapping("/nodes/{id}")
    public ResponseEntity deleteNode(@PathVariable Long id)
    {
        var node = repo.findById(id);

        if (node.isPresent())
        {
            repo.deleteById(id);

            return
                ResponseEntity
                .status(HttpStatus.OK)
                .body("Node with id = " + id + " is successful deleted.");
        }


        return
            ResponseEntity
            .status(HttpStatus.NOT_FOUND)
            .body("Node with id = " + id + " not found.");
    }

    @DeleteMapping("/nodes")
    public ResponseEntity deleteAll()
    {
        repo.deleteAll();

        return
            ResponseEntity
            .status(HttpStatus.OK)
            .body("Nodes table is cleared.");
    }
}
