package ru.nsu.fit.g15202.vakhrushev.dis.lab.Rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.g15202.vakhrushev.dis.lab.Model.NodeRepository;
import ru.nsu.fit.g15202.vakhrushev.dis.lab.Parse.NodeParser;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

@RestController
public class ParseController
{
    private final NodeRepository repo;
    private static final int COUNT_PER_ONCE = 1000;

    ParseController(NodeRepository repo) { this.repo = repo; }

    @PostMapping("/parse")
    public ResponseEntity parseNodes(@RequestParam String path)
    {
        var time = System.currentTimeMillis();

        try
        {
            var parser = new NodeParser(path);
            var size = COUNT_PER_ONCE;

            while (size == COUNT_PER_ONCE)
            {
                var nodes = parser.parseNodes(COUNT_PER_ONCE);
                repo.saveAll(nodes);

                size = nodes.size();
            }

            time = System.currentTimeMillis() - time;
        }
        catch (XMLStreamException e)
        {
            return
                    ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Invalid structure of OSM file");
        }
        catch (IOException e)
        {
            return
                    ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Not found OSM file from path: " + path);
        } catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }

        return
                ResponseEntity
                .status(HttpStatus.OK)
                .body("Elapsed time: " + time + " ms");
    }
}
